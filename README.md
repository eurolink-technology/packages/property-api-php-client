# Property API Client

A PHP client for [Eurolink's](https://www.eurolink.co/) Property API. This library is restricted for usage by approved clients who have been allowed access to the Property API.

Please direct any support enquiries to the [Eurolink support desk](https://hub.eurolink.co/) or email support@eurolink.co.

API documentation is online: [https://property-api.readme.io/](https://property-api.readme.io/)

## Setup
To install this package you need to be using Composer, and be comfortable using PHP namespaces.
Run the following command to pull the PHP package into your project’s ‘vendor’ folder.

```
composer require eurolink/property-api-php-client
```

## Usage

#### GET Properties

```php
<?php
    require 'vendor/autoload.php';

    use PropertyAPI\Client;

    $client = new Client([
        'accessToken' => '~~INSERT_ACCESS_TOKEN~~',
    ]);

    // additional GET parameters can be passed in (see full documentation for details)
    $collection = $client->getProperties([
        'size' => 5,
    ]);

    // returns total number of properties matching search criteria
    var_dump($collection->getTotal());

    // returns number of properties returned in this collection
    var_dump($collection->getCount());

    // returns array of objects
    var_dump($collection->getRows());

    // returns array of '\PropertyAPI\Property'
    var_dump($collection->getParsedRows());
?>
```

#### GET Property

```php
<?php
    require 'vendor/autoload.php';

    use PropertyAPI\Client;

    $client = new Client([
        'accessToken' => '~~INSERT_ACCESS_TOKEN~~',
    ]);

    // gets property by property ID
    $property = $client->getProperty(123456);

    // verify that property have successfully been returned
    var_dump($property->isValid());

    // test request by displaying Property ID (UUID)
    var_dump($property->getPropertyID());
?>
```

## Collection Methods

The following methods are available on an instance of `\PropertyAPI\Client`.

`getTotal()` - Returns the total number of properties matching the search criteria.

`getCount()` - Returns the number of properties in response.

`getRows()` - Get raw array of properties

`getParsedRows()` - Get array of properties, as instances of the `\PropertyAPI\Property` class.

`getRawResponse()` - Get raw JSON response. *Useful for debugging.*


## Property Class Methods

The following methods are available on an instance of `\PropertyAPI\Property`.

`getCompanyID()` - Returns the company UUID.

`getPropertyID()` - Returns the property UUID.

`getID()` - Returns the unique, numeric Property ID.

`getWebStatus()` - Returns the online marketing status/availability of the property.

`getBrochures()` - Returns array of brochures.

`getFeatures()` - Returns array of features.

`getAddress()` - Returns array of address lines.

`getAddressString()` - Returns address as one string, comma separated.

`getShortAddress()` - Returns property title of short address for advertising.

`getEPCImages()` - Returns array of EPC images.

`getEPCDocuments()` - Returns array of EPC documents.

`getFeesDescription()` - Returns fees.

`getFeesURL()` - Returns URL to fees information.

`getTenure()` - Returns tenure information.

`getTenureType()` - Returns tenure type.

`getFloorPlans()` - Returns array of floor plans.

`getCategory()` - Returns category (Sales or Lettings).

`isLettings()` - Returns true if category is Lettings.

`isSales()` - Returns true if category is Sales.

`getDescription()` - Returns property description.

`getCharge()` - Returns cost of service charge.

`isFeatured()` - Returns true if property is marked as featured.

`getFeaturedDate()` - Returns date that property was marked as featured.

`getUpdatedDate()` - Returns date property data was last updated.

`getCriteriaType()` - Returns classification of the property type.

`getAmount()` - Returns property price.

`getAvailableFromDate()` - Returns date property is available from.

`getTown()` - Returns town.

`getArea()` - Returns area.

`getSummaryDescription()` - Returns summary description.

`getRentPeriod()` - Returns rent period (eg: per week)

`getPropertyType()` - Returns property type.

`getClassification()` - Returns classification.

`isCommerical()` - Returns true if commercial property.

`getOutsideSpace()` - Returns information about the outside space.

`getParking()` - Returns information about the parking.

`getFloors()` - Returns the number of floors.

`getBedrooms()` - Returns the number of bedrooms.

`getBathrooms()` - Returns the number of bathrooms.

`getFurnished()` - Returns the furnished status.

`getSellingState()` - Returns the selling state.

`getMarketingDescription()` - Returns the marketing description.

`getMarketingDescriptionHTML()` - Returns the marketing description, including HTML.

`getNewProperty()` - Returns Y/N if property is new.

`getKeywords()` - Returns keywords.

`getGroundRent()` - Returns cost of ground rent.

`getNewHome()` - Returns true if  property is new home.

`getInsertDate()` - Returns date of first property insert.

`getURLs()` - Returns array of URLs.

`getOfficeID()` - Returns the associated office UUID.

`getOfficeName()` - Returns the associated office name.

`getOfficePhone()` - Returns the associated office phone number.

`getOfficeEmail()` - Returns the associated office email.

`getOfficeManager()` - Returns the associated office manager’s name.

`getOfficeURL()` - Returns the associated office website URL.

`getPhotos()` - Returns array of photos.

`getPostcode()` - Returns postcode.

`getLocation()` - Returns array of latitude and longitude.

`getLongitude()` - Returns longitude.

`getLatitude()` - Returns latitude.

`getVideoURLs()` - Returns array of video URLs.

Other data is available through the use of Magic Methods. For example:

`$property->URLs->URL2`


## Testing this package

#### Unit tests
Paste your API Access Token into the root `.env` file. See `.env.example`.

```bash
./vendor/bin/phpunit tests/
```

#### Visual tests
Paste your API Access Token into the root `.env` file. See `.env.example`.

```php
php example/simple.php
// or
php example/complex.php
```
