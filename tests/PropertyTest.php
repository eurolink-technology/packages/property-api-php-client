<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use PropertyAPI\Client;
    use PropertyAPI\Property;
    use Dotenv\Dotenv;

    final class PropertyTest extends TestCase
    {
        private $client;
        private $property;

        private function getAccessToken()
        {
            $dotenv = Dotenv::createImmutable(realpath(__DIR__ . '/..'));
            $dotenv->load();
            $dotenv->required('TOKEN')->notEmpty();
            $dotenv->required('PROPERTY_ID')->notEmpty();

            return $_ENV['TOKEN'];
        }

        private function setupClient()
        {
            $this->client = new Client([
                'accessToken' => $this->getAccessToken(),
            ]);
        }

        private function getProperty()
        {
           $this->property = $this->client->getProperty($_ENV['PROPERTY_ID']);
        }

        public function testCanGetProperties()
        {
            $this->setupClient();

            $this->getProperty();

            $this->assertInstanceOf(Property::class, $this->property);
            $this->assertIsString($this->property->getCompanyID());
            $this->assertEquals(36, strlen($this->property->getCompanyID()));
            $this->assertIsString($this->property->getPropertyID());
            $this->assertEquals(36, strlen($this->property->getPropertyID()));
            $this->assertIsInt($this->property->getID());
            $this->assertGreaterThan(1, $this->property->getID());
            $this->assertIsArray($this->property->getBrochures());
            $this->assertIsArray($this->property->getFeatures());
            $this->assertIsObject($this->property->getAddress());
            $this->assertIsArray($this->property->getAddressStringParts());
            $this->assertIsString($this->property->getAddressString());
            $this->assertIsArray($this->property->getEPCImages());
            $this->assertIsArray($this->property->getEPCDocuments());
            $this->assertIsArray($this->property->getFloorPlans());
            $this->assertIsBool($this->property->isFeatured());
            $this->assertIsArray($this->property->getURLs());
            $this->assertIsArray($this->property->getPhotos());
            $this->assertIsObject($this->property->getLocation());
            $this->assertIsArray($this->property->getVideoURLs());
            $this->assertIsObject($this->property->getOffice());
            $this->assertIsObject($this->property->getContact());
            $this->assertCount(0, $this->property->getFeatures(0));
            $this->assertCount(1, $this->property->getFeatures(1));
        }
    }