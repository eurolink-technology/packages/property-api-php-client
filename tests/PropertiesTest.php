<?php

    declare(strict_types=1);

    use Dotenv\Dotenv;
    use GuzzleHttp\Exception\RequestException;
    use PHPUnit\Framework\TestCase;
    use PropertyAPI\Client;
    use PropertyAPI\Property;

    final class PropertiesTest extends TestCase
    {
        private $client;
        private $properties;

        private function getAccessToken()
        {
            $dotenv = Dotenv::createImmutable(realpath(__DIR__ . '/..'));
            $dotenv->load();

            $dotenv->required('TOKEN')->notEmpty();

            return $_ENV['TOKEN'];
        }

        private function setupClient()
        {
            $this->client = new Client([
                'accessToken' => $this->getAccessToken(),
            ]);
        }

        private function getProperties()
        {
           $this->properties = $this->client->getProperties();
        }

        public function testIsAccessTokenNotEmpty()
        {
            $this->assertNotEmpty($this->getAccessToken());
        }

        public function testCannotInstanciatePropertiesClassWithMissingToken()
        {
            $this->expectExceptionMessage('Access token is missing.');

            new Client([]);
        }

        public function testCannotInstanciatePropertiesClassWithEmptyToken()
        {
            $this->expectExceptionMessage('Access token is empty.');

            new Client([
                'accessToken' => '',
            ]);
        }

        public function testCannotInstanciatePropertiesClassWithInvalidToken()
        {
            $this->expectException(RequestException::class);

            $this->client = new Client([
                'accessToken' => '~abcd~',
            ]);

            $this->getProperties();

            // test total
            $this->assertEquals($this->properties->getTotal(), 0);
            $this->assertEquals(count($this->properties->getRows()), 0);
            $this->assertEquals(count($this->properties->getParsedRows()), 0);

            // test rows
            $this->assertIsArray($this->properties->getRows());
            $this->assertIsArray($this->properties->getParsedRows());
        }

        public function testCanInstanciatePropertiesClass()
        {
            $this->setupClient();

            $this->assertInstanceOf(Client::class, $this->client);
        }

        public function testCanCreateCollection()
        {
            $this->setupClient();

            $this->getProperties();

            $this->assertInstanceOf(Client::class, $this->properties);
        }

        public function testCanGetPropertiesCount()
        {
            $this->setupClient();

            $this->getProperties();

            // test total
            $this->assertIsInt($this->properties->getTotal());
            $this->assertEquals(count($this->properties->getRows()), 1);
            $this->assertEquals(count($this->properties->getParsedRows()), 1);

            // test rows
            $this->assertIsArray($this->properties->getRows());
            $this->assertIsArray($this->properties->getParsedRows());
        }

        public function testCanGetProperties()
        {
            $this->setupClient();

            $this->getProperties();

            $property = $this->properties->getParsedRows()[0];

            $this->assertInstanceOf(Property::class, $property);
            $this->assertIsString($property->getCompanyID());
            $this->assertEquals(36, strlen($property->getCompanyID()));
            $this->assertIsString($property->getPropertyID());
            $this->assertEquals(36, strlen($property->getPropertyID()));
            $this->assertIsInt($property->getID());
            $this->assertGreaterThan(1, $property->getID());
            $this->assertIsArray($property->getBrochures());
            $this->assertIsArray($property->getFeatures());
            $this->assertIsObject($property->getAddress());
            $this->assertIsArray($property->getAddressStringParts());
            $this->assertIsString($property->getAddressString());
            $this->assertIsArray($property->getEPCImages());
            $this->assertIsArray($property->getEPCDocuments());
            $this->assertIsArray($property->getFloorPlans());
            $this->assertIsBool($property->isFeatured());
            $this->assertIsArray($property->getURLs());
            $this->assertIsArray($property->getPhotos());
            $this->assertIsObject($property->getLocation());
            $this->assertIsArray($property->getVideoURLs());
            $this->assertIsObject($property->getOffice());
            $this->assertIsObject($property->getContact());
            $this->assertCount(0, $property->getFeatures(0));
            $this->assertCount(1, $property->getFeatures(1));
        }
    }